import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';
import { SharedModule } from '../modules';

@NgModule({
    declarations: [NavbarComponent],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [NavbarComponent],
    entryComponents: [NavbarComponent],
})
export class NavbarModule { }
