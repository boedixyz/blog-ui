import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../_services';
import { Navbar } from '../_models';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    constructor(
        private navbarService: NavbarService,
    ) { }
    navbars: Array<Navbar>;

    ngOnInit(): void
    {
        this.getData();
    }

    /**
     * Get data
     */
    getData(): void
    {
        this.navbarService.get().then(navbars => this.navbars = navbars);
    }

}
