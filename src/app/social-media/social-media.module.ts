import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialMediaComponent } from './social-media.component';
import { SharedModule } from '../modules';

@NgModule({
    declarations: [SocialMediaComponent],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [SocialMediaComponent],
    entryComponents: [SocialMediaComponent],
})
export class SocialMediaModule { }
