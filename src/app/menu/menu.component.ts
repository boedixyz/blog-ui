import { Component, OnInit } from '@angular/core';
import { MenuService } from '../_services';
import { Menu } from '../_models';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

    constructor(
        private menuService: MenuService,
    ) { }
    menus: Array<Menu>;

    ngOnInit(): void
    {
        this.getData();
    }

    /**
     * Get data
     */
    getData(): void
    {
        this.menuService.get().then(menus => this.menus = menus);
    }

}
