import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu.component';
import { SharedModule } from '../modules';

@NgModule({
    declarations: [MenuComponent],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [MenuComponent],
    entryComponents: [MenuComponent],
})
export class MenuModule { }
