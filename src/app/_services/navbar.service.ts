import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Navbar } from '../_models';

@Injectable({
    providedIn: 'root'
})
export class NavbarService {

    constructor(
        private httpClient: HttpClient,
    ) { }

    /**
     * Get navbar
     */
    get(): Promise<Array<Navbar>>
    {
        return new Promise<Array<Navbar>>((resolve, reject) => {
            this.httpClient.get('navbar.php')
                .subscribe((response: any) => {
                    if (response.result) {
                        resolve(response.result.map(navbar => new Navbar(navbar)));
                    }
                }, reject);
        });
    }
}
