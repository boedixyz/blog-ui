import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Activity } from '../_models';

@Injectable({
    providedIn: 'root'
})
export class ActivityService {

    constructor(
        private httpClient: HttpClient,
    ) { }

    /**
     * Get activity
     */
    get(): Promise<Array<Activity>>
    {
        return new Promise<Array<Activity>>((resolve, reject) => {
            this.httpClient.get('activity.php')
                .subscribe((response: any) => {
                    if (response.result) {
                        resolve(response.result.map(activity => new Activity(activity)));
                    }
                }, reject);
        });
    }
}
