import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Menu } from '../_models';

@Injectable({
    providedIn: 'root'
})
export class MenuService {

    constructor(
        private httpClient: HttpClient,
    ) { }

    /**
     * Get menu
     */
    get(): Promise<Array<Menu>>
    {
        return new Promise<Array<Menu>>((resolve, reject) => {
            this.httpClient.get('menu.php')
                .subscribe((response: any) => {
                    if (response.result) {
                        resolve(response.result.map(menu => new Menu(menu)));
                    }
                }, reject);
        });
    }
}
