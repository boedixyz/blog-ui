import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video } from '../_models';

@Injectable({
    providedIn: 'root'
})
export class VideoService {

    constructor(
        private httpClient: HttpClient,
    ) { }

    /**
     * Get video
     */
    get(): Promise<Array<Video>>
    {
        return new Promise<Array<Video>>((resolve, reject) => {
            this.httpClient.get('video.php')
                .subscribe((response: any) => {
                    if (response.result) {
                        resolve(response.result.map(video => new Video(video)));
                    }
                }, reject);
        });
    }
}
