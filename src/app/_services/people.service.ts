import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { People } from '../_models';

@Injectable({
    providedIn: 'root'
})
export class PeopleService {

    constructor(
        private httpClient: HttpClient,
    ) { }

    /**
     * Get peoples
     */
    get(): Promise<Array<People>>
    {
        return new Promise<Array<People>>((resolve, reject) => {
            this.httpClient.get('people.php')
                .subscribe((response: any) => {
                    if (response.result) {
                        resolve(response.result.map(people => new People(people)));
                    }
                }, reject);
        });
    }
}
