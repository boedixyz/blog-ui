import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Channel } from '../_models';

@Injectable({
    providedIn: 'root'
})
export class ChannelService {

    constructor(
        private httpClient: HttpClient,
    ) { }

    /**
     * Get menu
     */
    get(): Promise<Array<Channel>>
    {
        return new Promise<Array<Channel>>((resolve, reject) => {
            this.httpClient.get('channel.php')
                .subscribe((response: any) => {
                    if (response.result) {
                        resolve(response.result.map(channel => new Channel(channel)));
                    }
                }, reject);
        });
    }
}
