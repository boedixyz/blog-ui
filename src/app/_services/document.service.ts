import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Document } from '../_models';

@Injectable({
    providedIn: 'root'
})
export class DocumentService {

    constructor(
        private httpClient: HttpClient,
    ) { }

    /**
     * Get document
     */
    get(): Promise<Array<Document>>
    {
        return new Promise<Array<Document>>((resolve, reject) => {
            this.httpClient.get('document.php')
                .subscribe((response: any) => {
                    if (response.result) {
                        resolve(response.result.map(document => new Document(document)));
                    }
                }, reject);
        });
    }
}
