import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { SharedModule } from '../modules';

@NgModule({
    declarations: [HeaderComponent],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [HeaderComponent],
    entryComponents: [HeaderComponent],
})
export class HeaderModule { }
