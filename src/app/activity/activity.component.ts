import { Component, OnInit } from '@angular/core';
import { ActivityService } from '../_services';
import { Activity } from '../_models';

@Component({
    selector: 'app-activity',
    templateUrl: './activity.component.html',
    styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {

    constructor(
        private activityService: ActivityService,
    ) { }
    activities: Array<Activity>;

    ngOnInit(): void
    {
        this.getData();
    }

    /**
     * Get data
     */
    getData(): void
    {
        this.activityService.get().then(activities => this.activities = activities);
    }

}
