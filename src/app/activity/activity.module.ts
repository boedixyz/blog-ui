import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityComponent } from './activity.component';
import { SharedModule } from '../modules';

@NgModule({
    declarations: [ActivityComponent],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [ActivityComponent],
    entryComponents: [ActivityComponent],
})
export class ActivityModule { }
