import { Component, OnInit } from '@angular/core';
import { PeopleService } from '../_services';
import { People } from '../_models';

@Component({
    selector: 'app-people',
    templateUrl: './people.component.html',
    styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {

    constructor(
        private peopleService: PeopleService,
    ) { }
    peoples: Array<People>;

    ngOnInit(): void
    {
        this.getData();
    }

    /**
     * Get data
     */
    getData(): void
    {
        this.peopleService.get().then(peoples => this.peoples = peoples);
    }

}
