import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeopleComponent } from './people.component';
import { SharedModule } from '../modules';

@NgModule({
    declarations: [PeopleComponent],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [PeopleComponent],
    entryComponents: [PeopleComponent],
})
export class PeopleModule { }
