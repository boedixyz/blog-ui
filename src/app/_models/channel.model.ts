export class Channel
{
    constructor(channel: ChannelInterface) {
        this.id = channel.id;
        this.name = channel.name;
    }

    id: string;
    name: string;
}

export interface ChannelInterface {
    id: string;
    name: string;
}
