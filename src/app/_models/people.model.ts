export class People
{
    constructor(people: PeopleInterface) {
        this.id = people.id;
        this.name = people.name;
        this.jobTitle = people.job_title;
        this.view = people.view;
    }
    id: number;
    name: string;
    jobTitle: string;
    view: string;
}

export interface PeopleInterface {
    id: number;
    name: string;
    job_title: string;
    view: string;
}
