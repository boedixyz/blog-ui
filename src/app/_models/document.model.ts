import { Author, AuthorInterface } from './author.model';

export class Document
{
    constructor(document: DocumentInterface) {
        this.id = document.id;
        this.title = document.title;
        this.image = document.image;
        this.view = document.view;
        this.author = (document.author) ? new Author(document.author) : undefined;
    }
    id: number;
    title: string;
    image: string;
    view: string;
    author: Author;
}

export interface DocumentInterface {
    id: number;
    title: string;
    image: string;
    view: string;
    author: AuthorInterface;
}
