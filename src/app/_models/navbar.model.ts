export class Navbar
{
    constructor(navbar: NavbarInterface) {
        this.id = navbar.id;
        this.label = navbar.label;
        this.url = navbar.url;
    }
    id: number;
    label: string;
    url: string;
}

export interface NavbarInterface {
    id: number;
    label: string;
    url: string;
}
