import { Author, AuthorInterface } from './author.model';

export class Video
{
    constructor(video: VideoInterface) {
        this.id = video.id;
        this.title = video.title;
        this.image = video.image;
        this.view = video.view;
        this.author = (video.author) ? new Author(video.author) : undefined;
    }
    id: number;
    title: string;
    image: string;
    view: string;
    author: Author;
}

export interface VideoInterface {
    id: number;
    title: string;
    image: string;
    view: string;
    author: AuthorInterface;
}
