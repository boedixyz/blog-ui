import { Author, AuthorInterface } from './author.model';

export class Activity
{
    constructor(activity: ActivityInterface) {
        this.id = activity.id;
        this.title = activity.title;
        this.body = activity.body;
        this.createdAt = activity.created_at;
        this.author = (activity.author) ? new Author(activity.author) : undefined;
    }
    id: number;
    title: string;
    body: string;
    createdAt: string;
    author: Author;
}

export interface ActivityInterface {
    id: number;
    title: string;
    body: string;
    created_at: string;
    author: AuthorInterface;
}
