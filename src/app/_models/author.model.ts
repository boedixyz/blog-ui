export class Author
{
    constructor(author: AuthorInterface) {
        this.name = author.name;
        this.email = author.email;
    }
    name: string;
    email: string;
}

export interface AuthorInterface {
    name: string;
    email: string;
}
