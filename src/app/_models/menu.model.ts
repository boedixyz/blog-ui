export class Menu
{
    constructor(menu: MenuInterface) {
        this.id = menu.id;
        this.label = menu.label;
        this.url = menu.url;
    }
    id: number;
    label: string;
    url: string;
}

export interface MenuInterface {
    id: number;
    label: string;
    url: string;
}
