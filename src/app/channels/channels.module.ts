import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChannelsComponent } from './channels.component';
import { SharedModule } from '../modules';

@NgModule({
    declarations: [ChannelsComponent],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [ChannelsComponent],
    entryComponents: [ChannelsComponent],
})
export class ChannelsModule { }
