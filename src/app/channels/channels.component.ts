import { Component, OnInit } from '@angular/core';
import { ChannelService } from '../_services';
import { Channel } from '../_models';

@Component({
    selector: 'app-channels',
    templateUrl: './channels.component.html',
    styleUrls: ['./channels.component.scss']
})
export class ChannelsComponent implements OnInit {

    constructor(
        private channelService: ChannelService,
    ) { }
    channels: Array<Channel>;

    ngOnInit(): void
    {
        this.getData();
    }

    /**
     * Get data
     */
    getData(): void
    {
        this.channelService.get().then(channels => this.channels = channels);
    }

}
