import { Component, OnInit } from '@angular/core';
import { DocumentService } from '../_services';
import { Document } from '../_models';

@Component({
    selector: 'app-documents',
    templateUrl: './documents.component.html',
    styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {

    constructor(
        private documentService: DocumentService,
    ) { }
    documents: Array<Document>;

    ngOnInit(): void
    {
        this.getData();
    }

    /**
     * Get data
     */
    getData(): void
    {
        this.documentService.get().then(documents => this.documents = documents);
    }

}
