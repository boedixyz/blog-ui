import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentsComponent } from './documents.component';
import { SharedModule } from '../modules';

@NgModule({
    declarations: [DocumentsComponent],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [DocumentsComponent],
    entryComponents: [DocumentsComponent],
})
export class DocumentsModule { }
