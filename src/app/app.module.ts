import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { HeaderModule } from './header/header.module';
import { NavbarModule } from './navbar/navbar.module';
import { VideosModule } from './videos/videos.module';
import { PeopleModule } from './people/people.module';
import { ActivityModule } from './activity/activity.module';
import { ChannelsModule } from './channels/channels.module';
import { DocumentsModule } from './documents/documents.module';
import { SocialMediaModule } from './social-media/social-media.module';
import { MenuModule } from './menu/menu.module';
import { FooterModule } from './footer/footer.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FlexLayoutModule,
        HeaderModule,
        NavbarModule,
        VideosModule,
        PeopleModule,
        ActivityModule,
        ChannelsModule,
        DocumentsModule,
        SocialMediaModule,
        MenuModule,
        FooterModule,
    ],
    providers: [
        //
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
