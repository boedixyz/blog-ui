import { Component, OnInit } from '@angular/core';
import { VideoService } from '../_services';
import { Video } from '../_models';

@Component({
    selector: 'app-videos',
    templateUrl: './videos.component.html',
    styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {

    constructor(
        private videoService: VideoService,
    ) { }
    videos: Array<Video>;

    ngOnInit(): void
    {
        this.getData();
    }

    /**
     * Get data
     */
    getData(): void
    {
        this.videoService.get().then(videos => this.videos = videos);
    }

}
