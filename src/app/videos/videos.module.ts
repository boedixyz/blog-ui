import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideosComponent } from './videos.component';
import { SharedModule } from '../modules';

@NgModule({
    declarations: [VideosComponent],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [VideosComponent],
    entryComponents: [VideosComponent],
})
export class VideosModule { }
