import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpIntercept } from '../../_helpers';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule,
        FlexLayoutModule,
        HttpClientModule,
    ],
    exports: [
        BrowserModule,
        RouterModule,
        FlexLayoutModule,
        HttpClientModule,
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpIntercept,
            multi: true,
        }
    ],
})
export class SharedModule { }
